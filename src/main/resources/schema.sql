-- Drop table

-- DROP TABLE aves.company

CREATE TABLE aves.company (
	id serial NOT NULL,
	company_name text NOT NULL,
	deleted bool NOT NULL DEFAULT false,
	CONSTRAINT company_pkey PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
) ;

-- Drop table

-- DROP TABLE aves.login

CREATE TABLE aves.login (
	user_id uuid NOT NULL,
	username varchar(256) NOT NULL,
	password varchar(256) NOT NULL,
	"role" varchar(64) NULL,
	created_ts timestamp NULL DEFAULT CURRENT_TIMESTAMP,
	modified_ts timestamp NULL DEFAULT CURRENT_TIMESTAMP,
	modified_by varchar(64) NULL,
	created_by varchar(64) NULL
)
WITH (
	OIDS=FALSE
) ;

-- Drop table

-- DROP TABLE aves.team

CREATE TABLE aves.team (
	id serial NOT NULL,
	"name" text NOT NULL,
	parent_team int4 NULL,
	deleted bool NOT NULL DEFAULT false,
	CONSTRAINT team_pkey PRIMARY KEY (id),
	CONSTRAINT team_team_fk FOREIGN KEY (parent_team) REFERENCES aves.team(id)
)
WITH (
	OIDS=FALSE
) ;

-- Drop table

-- DROP TABLE aves.team_membership

CREATE TABLE aves.team_membership (
	id serial NOT NULL,
	subteam_id int4 NOT NULL,
	user_id uuid NOT NULL,
	deleted bool NOT NULL DEFAULT false,
	CONSTRAINT team_membership_pkey PRIMARY KEY (id),
	CONSTRAINT subteam_id FOREIGN KEY (subteam_id) REFERENCES aves.team(id)
)
WITH (
	OIDS=FALSE
) ;
CREATE INDEX fki_subteam_id ON aves.team_membership (subteam_id) ;
CREATE INDEX fki_user_id_fk ON aves.team_membership (user_id) ;

-- Drop table

-- DROP TABLE aves."user"

CREATE TABLE aves."user" (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	first_name text NULL,
	last_name text NULL,
	email text NULL,
	phone text NULL,
	location text NULL,
	company int4 NULL,
	"position" text NULL,
	deleted bool NOT NULL DEFAULT false,
	CONSTRAINT user_email_key UNIQUE (email),
	CONSTRAINT user_pkey PRIMARY KEY (id),
	CONSTRAINT company_id_fk FOREIGN KEY (company) REFERENCES aves.company(id)
)
WITH (
	OIDS=FALSE
) ;
