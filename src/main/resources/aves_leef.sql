--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4 (Ubuntu 10.4-0ubuntu0.18.04)
-- Dumped by pg_dump version 10.4 (Ubuntu 10.4-0ubuntu0.18.04)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: aves; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA aves;


ALTER SCHEMA aves OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: company; Type: TABLE; Schema: aves; Owner: postgres
--

CREATE TABLE aves.company (
    id integer NOT NULL,
    company_name text NOT NULL,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE aves.company OWNER TO postgres;

--
-- Name: company_id_seq; Type: SEQUENCE; Schema: aves; Owner: postgres
--

CREATE SEQUENCE aves.company_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aves.company_id_seq OWNER TO postgres;

--
-- Name: company_id_seq; Type: SEQUENCE OWNED BY; Schema: aves; Owner: postgres
--

ALTER SEQUENCE aves.company_id_seq OWNED BY aves.company.id;


--
-- Name: login; Type: TABLE; Schema: aves; Owner: postgres
--

CREATE TABLE aves.login (
    user_id uuid NOT NULL,
    username character varying(256) NOT NULL,
    password character varying(256) NOT NULL,
    role character varying(64),
    created_ts timestamp without time zone DEFAULT now(),
    modified_ts timestamp without time zone DEFAULT now(),
    modified_by character varying(64),
    created_by character varying(64)
);


ALTER TABLE aves.login OWNER TO postgres;

--
-- Name: team; Type: TABLE; Schema: aves; Owner: postgres
--

CREATE TABLE aves.team (
    id integer NOT NULL,
    name text NOT NULL,
    parent_team integer,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE aves.team OWNER TO postgres;

--
-- Name: team_id_seq; Type: SEQUENCE; Schema: aves; Owner: postgres
--

CREATE SEQUENCE aves.team_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aves.team_id_seq OWNER TO postgres;

--
-- Name: team_id_seq; Type: SEQUENCE OWNED BY; Schema: aves; Owner: postgres
--

ALTER SEQUENCE aves.team_id_seq OWNED BY aves.team.id;


--
-- Name: team_membership; Type: TABLE; Schema: aves; Owner: postgres
--

CREATE TABLE aves.team_membership (
    id integer NOT NULL,
    subteam_id integer NOT NULL,
    user_id uuid NOT NULL,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE aves.team_membership OWNER TO postgres;

--
-- Name: team_membership_id_seq; Type: SEQUENCE; Schema: aves; Owner: postgres
--

CREATE SEQUENCE aves.team_membership_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aves.team_membership_id_seq OWNER TO postgres;

--
-- Name: team_membership_id_seq; Type: SEQUENCE OWNED BY; Schema: aves; Owner: postgres
--

ALTER SEQUENCE aves.team_membership_id_seq OWNED BY aves.team_membership.id;


--
-- Name: user; Type: TABLE; Schema: aves; Owner: postgres
--

CREATE TABLE aves."user" (
    id uuid DEFAULT aves.uuid_generate_v4() NOT NULL,
    first_name text,
    last_name text,
    email text,
    phone text,
    location text,
    company integer,
    "position" text,
    deleted boolean DEFAULT false NOT NULL,
    deleted_by_user_id uuid
);


ALTER TABLE aves."user" OWNER TO postgres;

--
-- Name: company id; Type: DEFAULT; Schema: aves; Owner: postgres
--

ALTER TABLE ONLY aves.company ALTER COLUMN id SET DEFAULT nextval('aves.company_id_seq'::regclass);


--
-- Name: team id; Type: DEFAULT; Schema: aves; Owner: postgres
--

ALTER TABLE ONLY aves.team ALTER COLUMN id SET DEFAULT nextval('aves.team_id_seq'::regclass);


--
-- Name: team_membership id; Type: DEFAULT; Schema: aves; Owner: postgres
--

ALTER TABLE ONLY aves.team_membership ALTER COLUMN id SET DEFAULT nextval('aves.team_membership_id_seq'::regclass);


--
-- Data for Name: company; Type: TABLE DATA; Schema: aves; Owner: postgres
--

COPY aves.company (id, company_name, deleted) FROM stdin;
1	The Buffalo Group	f
2	Falcon Logic	f
3	Techflow	f
4	KForce	f
5	iSenpai	f
\.


--
-- Data for Name: login; Type: TABLE DATA; Schema: aves; Owner: postgres
--

COPY aves.login (user_id, username, password, role, created_ts, modified_ts, modified_by, created_by) FROM stdin;
9b618532-02b7-4615-8b68-96976fd27362	bugs	{noop}pass	ADMIN	2018-06-28 10:12:18.476922	2018-06-28 10:12:18.476922	leef	leef
30521e65-a218-49d0-ba96-14269cfeac4e	tweety	{noop}pass	MANAGER	2018-06-28 10:12:18.476922	2018-06-28 10:12:18.476922	leef	leef
a9c83898-871b-4258-b0f9-c37fd5ebc74f	daffy	{noop}pass	USER	2018-06-28 10:12:18.476922	2018-06-28 10:12:18.476922	leef	leef
b492f3fb-b158-4439-89d3-3936c74a18f9	tasmanian	{noop}pass	USER	2018-06-28 10:12:18.476922	2018-06-28 10:12:18.476922	leef	leef
37844865-7c70-40f3-a944-3d46d5badca2	porky	{noop}pass	USER	2018-06-28 10:12:18.476922	2018-06-28 10:12:18.476922	leef	leef
7ef15427-4085-48d5-bb45-a99556c842e5	marvin	{noop}pass	USER	2018-06-28 10:12:18.476922	2018-06-28 10:12:18.476922	leef	leef
270ca837-bafe-4163-8270-17576b23f8e4	elmer	{noop}pass	USER	2018-06-28 10:12:18.476922	2018-06-28 10:12:18.476922	leef	leef
542042c4-5102-4bf5-823a-0bf35688b758	yosemite	{noop}pass	USER	2018-06-28 10:12:18.476922	2018-06-28 10:12:18.476922	leef	leef
e4062b9f-3023-40dd-bfc9-552171db3e30	wiley	{noop}pass	USER	2018-06-28 10:12:18.476922	2018-06-28 10:12:18.476922	leef	leef
6085c1de-afd9-45ab-bb9e-ed669ce649b1	road	{noop}pass	USER	2018-06-28 10:12:18.476922	2018-06-28 10:12:18.476922	leef	leef
\.


--
-- Data for Name: team; Type: TABLE DATA; Schema: aves; Owner: postgres
--

COPY aves.team (id, name, parent_team, deleted) FROM stdin;
1	NFL	\N	f
2	Redskins	1	f
3	Seahawks	1	f
4	Bears	1	f
5	Falcons	1	f
6	Eagles	1	f
7	Dolphins	1	f
8	NBA	\N	f
9	Bulls	8	f
10	Hawks	8	f
11	Pelicans	8	f
12	Bucks	8	f
13	Grizzlies	8	f
\.


--
-- Data for Name: team_membership; Type: TABLE DATA; Schema: aves; Owner: postgres
--

COPY aves.team_membership (id, subteam_id, user_id, deleted) FROM stdin;
1	2	9b618532-02b7-4615-8b68-96976fd27362	f
2	2	30521e65-a218-49d0-ba96-14269cfeac4e	f
3	2	a9c83898-871b-4258-b0f9-c37fd5ebc74f	f
4	2	b492f3fb-b158-4439-89d3-3936c74a18f9	f
5	2	37844865-7c70-40f3-a944-3d46d5badca2	f
6	2	7ef15427-4085-48d5-bb45-a99556c842e5	f
7	2	270ca837-bafe-4163-8270-17576b23f8e4	f
8	2	542042c4-5102-4bf5-823a-0bf35688b758	f
9	2	e4062b9f-3023-40dd-bfc9-552171db3e30	f
10	2	6085c1de-afd9-45ab-bb9e-ed669ce649b1	f
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: aves; Owner: postgres
--

COPY aves."user" (id, first_name, last_name, email, phone, location, company, "position", deleted, deleted_by_user_id) FROM stdin;
9b618532-02b7-4615-8b68-96976fd27362	Bugs	Bunny	bb@toon.com	111-222-1111	Reston	1	SW Dev	f	\N
30521e65-a218-49d0-ba96-14269cfeac4e	Tweety	Bird	tb@toon.com	111-222-2222	Sterling	1	QA	f	\N
a9c83898-871b-4258-b0f9-c37fd5ebc74f	Daffy	Duck	dd@toon.com	111-222-3333	Sterling	2	BA	f	\N
b492f3fb-b158-4439-89d3-3936c74a18f9	Tasmanian	Devil	td@toon.com	111-222-4444	Reston	1	DBA	f	\N
37844865-7c70-40f3-a944-3d46d5badca2	Porky	Pig	pp@@toon.com	111-222-5555	Sterling	2	SW Dev	f	\N
7ef15427-4085-48d5-bb45-a99556c842e5	Marvin	Martian	mm@@toon.com	111-222-6666	Sterling	2	SW Dev	f	\N
270ca837-bafe-4163-8270-17576b23f8e4	Elmer	Fudd	ef@toon.com	111-222-7777	Sterling	3	SW Dev	f	\N
542042c4-5102-4bf5-823a-0bf35688b758	Yosemite	Sam	ys@toon.com	111-222-8888	Sterling	4	DBA	f	\N
e4062b9f-3023-40dd-bfc9-552171db3e30	Wiley	Coyote	wc@toon.com	111-222-9999	Sterling	4	QA	f	\N
6085c1de-afd9-45ab-bb9e-ed669ce649b1	Road	Runnery	rr@toon.com	111-222-0000	Reston	5	Manager	f	\N
\.


--
-- Name: company_id_seq; Type: SEQUENCE SET; Schema: aves; Owner: postgres
--

SELECT pg_catalog.setval('aves.company_id_seq', 5, true);


--
-- Name: team_id_seq; Type: SEQUENCE SET; Schema: aves; Owner: postgres
--

SELECT pg_catalog.setval('aves.team_id_seq', 13, true);


--
-- Name: team_membership_id_seq; Type: SEQUENCE SET; Schema: aves; Owner: postgres
--

SELECT pg_catalog.setval('aves.team_membership_id_seq', 10, true);


--
-- Name: company company_pkey; Type: CONSTRAINT; Schema: aves; Owner: postgres
--

ALTER TABLE ONLY aves.company
    ADD CONSTRAINT company_pkey PRIMARY KEY (id);


--
-- Name: team_membership team_membership_pkey; Type: CONSTRAINT; Schema: aves; Owner: postgres
--

ALTER TABLE ONLY aves.team_membership
    ADD CONSTRAINT team_membership_pkey PRIMARY KEY (id);


--
-- Name: team team_pkey; Type: CONSTRAINT; Schema: aves; Owner: postgres
--

ALTER TABLE ONLY aves.team
    ADD CONSTRAINT team_pkey PRIMARY KEY (id);


--
-- Name: user user_email_key; Type: CONSTRAINT; Schema: aves; Owner: postgres
--

ALTER TABLE ONLY aves."user"
    ADD CONSTRAINT user_email_key UNIQUE (email);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: aves; Owner: postgres
--

ALTER TABLE ONLY aves."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: fki_subteam_id; Type: INDEX; Schema: aves; Owner: postgres
--

CREATE INDEX fki_subteam_id ON aves.team_membership USING btree (subteam_id);


--
-- Name: fki_user_id_fk; Type: INDEX; Schema: aves; Owner: postgres
--

CREATE INDEX fki_user_id_fk ON aves.team_membership USING btree (user_id);


--
-- Name: user company_id_fk; Type: FK CONSTRAINT; Schema: aves; Owner: postgres
--

ALTER TABLE ONLY aves."user"
    ADD CONSTRAINT company_id_fk FOREIGN KEY (company) REFERENCES aves.company(id);


--
-- Name: team_membership subteam_id; Type: FK CONSTRAINT; Schema: aves; Owner: postgres
--

ALTER TABLE ONLY aves.team_membership
    ADD CONSTRAINT subteam_id FOREIGN KEY (subteam_id) REFERENCES aves.team(id);


--
-- Name: team team_team_fk; Type: FK CONSTRAINT; Schema: aves; Owner: postgres
--

ALTER TABLE ONLY aves.team
    ADD CONSTRAINT team_team_fk FOREIGN KEY (parent_team) REFERENCES aves.team(id);


--
-- PostgreSQL database dump complete
--

