package com.deea.aves.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@RestController

public class Auth {

    @Autowired
    private HttpServletRequest request; 

    @Autowired
    private HttpServletResponse response; 
   
    @GetMapping("/user") 
    public String getUser() {
        Principal principal = request.getUserPrincipal();
        return principal.getName();
    }

    @GetMapping("/role") 
    public String role() {
        return request.isUserInRole("ADMIN") ? "ADMIN" : request.isUserInRole("MANAGER") ? "MANAGER" : "USER";
    }

    public boolean isManager() {
        return request.isUserInRole("MANAGER");
    }
    public boolean isAdmin() {
        boolean isAdmin = request.isUserInRole("ADMIN");
        return isAdmin;
    }

    public boolean notAdminSendUnauthorized() {
        boolean isAdmin = this.isAdmin();
        if ( !isAdmin ) {
            try {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            } catch ( Exception ex) {}
        }
        return isAdmin;
    }


    @GetMapping("/admin_test")
    public String responseX(HttpServletResponse response) throws Exception {
        if ( !isAdmin() ) {
            response.sendError(response.SC_UNAUTHORIZED);
            return null;
        }
        return "GOOD";
    }
}