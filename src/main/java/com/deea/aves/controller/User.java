package com.deea.aves.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.ibatis.session.SqlSession;


@RestController
@RequestMapping("/User")
public class User {

    @Autowired
    private SqlSession sqlSession;

    @Autowired
    Auth auth;

    @GetMapping() 
    public List<Map<String,Object>> getAll() {

        List<Map<String,Object>> usersList = sqlSession.selectList("User.select");
        boolean isAdmin = auth.isAdmin();
        List<Map<String,Object>> xList = usersList.stream().map( mm -> { mm.put("is_deletable",isAdmin); return mm; }).collect(Collectors.toList());

        if ( !auth.isAdmin() && auth.isManager() ) {

            Map<String,String> qmap = new HashMap<>();
            qmap.put("username", auth.getUser());
            List<String> managerTeams = sqlSession.selectList( "User_Team.managerTeam" , qmap);
            xList = usersList.stream()
            .map( mm -> { mm.put("is_deletable",managerTeams.contains(mm.get("id").toString())); return mm; })
            .collect(Collectors.toList());

        }


        return xList;


    }


    @GetMapping("{id}") 
    public java.util.HashMap<String,Object> getById(@PathVariable("id") Object id) {
        Map<String,Object> q = new HashMap<>();
        q.put("id",id);
        return sqlSession.selectOne("User.select",q);
    }

    @DeleteMapping("{id}")
    public String soft_delete( @PathVariable("id") Integer id) {
        sqlSession.update("User.delete", id);
        return "success";
    }

    @PostMapping()
    public Map<String,Object> insert( @RequestBody Map<String,Object> m ) {
        sqlSession.insert("User.insert", m);
        return m;
    }

    @PutMapping()
    public String update( @RequestBody Map m ) {
        sqlSession.insert("User.update", m);
        return "success";
    }

}