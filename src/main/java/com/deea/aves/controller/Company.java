package com.deea.aves.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.HashMap;
import java.util.List;
import java.util.Map;


import javax.servlet.http.HttpServletResponse;

import org.apache.ibatis.session.SqlSession;

@RestController
@RequestMapping("/Company")
public class Company {

    @Autowired
    private SqlSession sqlSession;

    @Autowired
    private Auth auth; 

    @Autowired
    HttpServletResponse response;
   
    @GetMapping() 
    public List<java.util.HashMap<String,Object>> getAll() {

        List<java.util.HashMap<String,Object>> result =  sqlSession.selectList("Company.select");
        return result;
        
    }

    @GetMapping("{id}") 
    public java.util.HashMap<String,Object> getById(@PathVariable("id") Integer id) {
        Map<String,Integer> q = new HashMap<>();
        q.put("id",id);
        return sqlSession.selectOne("Company.select",q);
    }

    @DeleteMapping("{id}")
    public String soft_delete( @PathVariable("id") Integer id) throws Exception {

        if ( !auth.notAdminSendUnauthorized() ) {
            return null;
        }
        sqlSession.update("Company.delete", id);
        return "success";
    }

    @PostMapping()
    public Map<String,Object> insert( @RequestBody Map<String,Object> m ) throws Exception {
        if ( !auth.notAdminSendUnauthorized() ) {
            return null;
        }
        sqlSession.insert("Company.insert", m);
        return m;
    }

    @PutMapping("{id}")
    public String update( @PathVariable Integer id, @RequestBody Map<String,Object> m ) throws Exception {
        if ( !auth.notAdminSendUnauthorized() ) {
            return null;
        }
        m.put("id", id);
        sqlSession.insert("Company.update", m);
        return "success";
    }

}