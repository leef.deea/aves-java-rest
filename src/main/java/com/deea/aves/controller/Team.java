package com.deea.aves.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;


@RestController
@RequestMapping("/Team")

public class Team {

    @Autowired
    private SqlSession sqlSession;

    @Autowired
    private Auth auth;

    @GetMapping() 
    public List<java.util.HashMap<String,Object>> getAll() {
        return sqlSession.selectList("Team.select");
    }

    @GetMapping("{id}") 
    public java.util.HashMap<String,Object> getById(@PathVariable("id") Integer id) {
        Map<String,Integer> q = new HashMap<>();
        q.put("id",id);
        return sqlSession.selectOne("Team.select",q);
    }

    @DeleteMapping("{id}")
    public String soft_delete( @PathVariable("id") Integer id) {

        if ( !auth.notAdminSendUnauthorized() ) {
            return null;
        }

        Map m = this.getById(id);
        if ( m.get("child") == null ) {
            sqlSession.update("Team.delete", id);
            return "success";
        }
        return "failed, can not remove parent team without removing child team first";
    }

    @PostMapping()
    public Map<String,Object> insert( @RequestBody Map<String,Object> m ) {

        if ( !auth.notAdminSendUnauthorized() ) {
            return null;
        }
        sqlSession.insert("Team.insert", m);
        return m;
    }

    @PutMapping("{id}")
    public String update( @PathVariable("id") Integer id, @RequestBody Map<String,Object> m ) {


        if ( !auth.notAdminSendUnauthorized() ) {
            return null;
        }
        
        m.put("id",id);

        Map<String,Object> team = this.getById(id);
        if ( team.get("parent_team") == null 
            && team.get("child") != null 
            && m.get("parent_team") != null) {
            
            // has child and trying to make it to child.
            return "failure, can not assign a parent_team to a parent with childs.";
        }

        sqlSession.update("Team.update", m);
        return "success";
    }

}