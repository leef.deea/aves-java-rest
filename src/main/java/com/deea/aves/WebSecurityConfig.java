package com.deea.aves;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;


@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    SqlSession sqlSession;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .csrf().disable()
            .authorizeRequests()
            .antMatchers(HttpMethod.GET, "/Company/**").permitAll()
            .antMatchers(HttpMethod.GET, "/Team/**").permitAll()
            .antMatchers(HttpMethod.GET, "/User/**").permitAll()
            .anyRequest().authenticated()
            .and()
            .formLogin().defaultSuccessUrl("/User")
                .permitAll()
                .and()
            .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .permitAll();
    }

    @Bean
    @Override
    public UserDetailsService userDetailsService() {

        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();

        List<Map> login = sqlSession.selectList("Login.select");

        for( Map l : login ) {
            manager.createUser(
                User.withUsername((String)l.get("username"))
                    .password((String)l.get("password"))
                    .roles((String)l.get("role"))
                    .build()
            );
        }

        
        // InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
        // manager.createUser(
        //     User.withDefaultPasswordEncoder()
        //     .username("user")
        //     .password("pass")
        //     .roles("USER")
        //     .build()   
        // );
        // manager.createUser(
        //     User.withDefaultPasswordEncoder()
        //     .username("admin")
        //     .password("pass")
        //     .roles("ADMIN")
        //     .build()  
        // );

        // BCryptPasswordEncoder encoder = passwordEncoder();
        // String encodedPassword = encoder.encode("pass");

        // manager.createUser(
        //     User
        //     .withUsername("user")
        //     .password(encodedPassword)
        //     .roles("ADMIN")
        //     .passwordEncoder(encoder::encode)    
        //     .build()
        // );

        return manager;
    }

}